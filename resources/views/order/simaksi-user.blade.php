<!DOCTYPE html>
 <html>
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Surat Ijin Mendaki</title>

 </head>
 <body style="padding: 10px;">
  
   
    <table width="100%">
        <tr>
            
            <td width="85" align="center">
                <H5><u>SURAT IJIN MENDAKI</u></H5>
               
            </td>

        </tr>
    </table>
    
    <p>Bersama surat ini diberitahukan bahwa :</p>
    <table class="table">
        <tr>
            <td>Nomor Registrasi </td>
            <th>:</th>
            <td>{{ $data->order_code }}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <th>:</th>
            <td>{{ $data->name }}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <th>:</th>
            <td>{{ $data->gender }}</td>
        </tr> 
        <tr>
            <td>No Hp</td>
            <th>:</th>
            <td>{{ $data->no_hp }}</td>
        </tr> 
         <tr>
            <td>Alamat</td>
            <th>:</th>
            <td>{{ $data->alamat }}</td>
        </tr>      
        <tr>
            <td>Mendaki Dari Tanggal s/d Tanggal</td>
            <th>:</th>
            <td>{{date('d-m-Y', strtotime($data->tgl_naik)) }} s/d {{date('d-m-Y', strtotime($data->tgl_turun))}}</td>
        </tr>                        
    </table> 

    <br>
    <p>Hasil MCU :</p>
    <table class="table">
        <tr>
            <td>Tinggi Badan </td>
            <th>:</th>
            <td>{{ $mcu->tinggi_badan }} cm</td>
        </tr>
        <tr>
            <td>Berat Badan </td>
            <th>:</th>
            <td>{{ $mcu->berat_badan }} kg</td>
        </tr>
        <tr>
            <td>Tensi Darah </td>
            <th>:</th>
            <td>{{ $mcu->tensi_darah }} </td>
        </tr>
        <tr>
            <td>Keterangan </td>
            <th>:</th>
            <td>{{ $mcu->keterangan }} </td>
        </tr>
        <tr>
            <td>Diperiksa oleh </td>
            <th>:</th>
            <td>{{ $mcu->diperiksa_oleh }} </td>
        </tr>         
    </table> 

    <p>Sewa Barang :</p>
    <table class="table">
 
        @forelse($sewa as $s)
            <tr>
                <td>- {{$s->nama_barang}}</td>
                <th></th>
                <td>{{$s->qty}}</td>
            </tr>
        @empty
        <tr>
            <td>-</td>
            <th>-</th>
        </tr>   
        @endforelse
               
    </table> 

  <br><br><br><br>
  <p>"Selamat Mendaki Gunung Gede Pangrango".</p>

 </body>
 </html>


 <script>
    window.print();
    $(document).ready(function(){

    });
</script>