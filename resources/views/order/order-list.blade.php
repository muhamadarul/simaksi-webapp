@extends("crudbooster::admin_template")

@push('head')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="http://cdn.datatables.net/responsive/1.0.2/css/dataTables.responsive.css"/>
@push



@section('content')
  <!-- tombol pemicu -->
  <button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#modalExportPdf">
      Export
  </button>
  <br>
  <br>
  <div class="row">
    <form method="GET" action="{{ route('orderList') }}">
      <div class="col-md-4">
        <div class="col-auto">
          <label>Search</label>
          <input name="keyword" value="{{ Request::input('keyword') ? Request::input('keyword') : '' }}" type="text" class="form-control" placeholder="Search... Order code or Name" style="float:right;">
        </div>
      </div>
      <div class="col-md-2">
        <div class="col-auto">
          <label>Tanggal Awal (Tgl Naik)</label>
          <input name="start_date" value="{{ Request::input('start_date') ? Request::input('start_date') : '' }}" placeholder="Start Date" type="date" class="form-control" style="float:right;">
        </div>
      </div>
      <div class="col-md-2">
        <div class="col-auto">
          <label>Tanggal Akhir (Tgl Naik)</label>
          <input name="end_date" value="{{ Request::input('end_date') ? Request::input('end_date') : '' }}" placeholder="end_date" type="date" class="form-control" style="float:right;">
        </div>
      </div>
      <div class="col-md-2">
        <div class="col-auto" style="float:left;">
          <label for="">Action</label><br>
          <button type="submit" class="btn btn-primary mb-3"><i class="fa fa-search"></i>&nbsp; Search</button>&nbsp;
          <a class="btn btn-success mb-3" href="{{ route('orderList') }}"><i class="fa fa-refresh"></i>&nbsp; Reset</a>
        </div>
      </div>
    </form>
  </div>
  <br><br>
  <div class="box box-info">

  <div class="box-body">
    <table id="dataTable" class="table table-bordered table-striped">
      <thead>
        <th>No</th>
        <th>Pendaki</th>
        <th>Email</th>
        <th>Order Code</th>
        <th>Tgl Naik</th>
        <th>Tgl Turun</th>
        <th>Persetujuan</th>
        <th>Disetujui Oleh</th>
        <th>Check in</th>
        <th>Check out</th>
        <th>Hasil MCU</th>
        <th>Sewa Barang</th>
      </thead>
      @php
        $no = 1;
      @endphp
      <tbody>
        @forelse ($data as $key => $value)
          <tr>
            <td>{{$data->firstItem()+$key}}</td>
            <td>{{$value->pendaki}}</td>
            <td>{{$value->email_pendaki}}</td>
            <td>{{$value->order_code}}</td>
            <td>{{$value->tgl_naik}}</td>
            <td>{{$value->tgl_turun}}</td>
            @if ($value->persetujuan == 1)
            <td>Disetujui</td>
            @else
              <td>-</td>
            @endif
            <td>{{$value->disetujui_oleh}}</td>
            <td>{{$value->check_in}}</td>
            <td>{{$value->check_out}}</td>
            @php
            $str = "";
            $str_barang = "";

            $mcu_data = DB::table('mcu_users')->where('order_id', $value->id)->first();

            if($mcu_data){
                $str .= "Tensi darah : {$mcu_data->tensi_darah}, ";
                $str .= "Tinggi badan : {$mcu_data->tinggi_badan}, ";
                $str .= "Berat badan : {$mcu_data->berat_badan}, ";
                $str .= "Keterangan : {$mcu_data->keterangan}, ";
                $str .= "Diperiksa oleh : {$mcu_data->diperiksa_oleh}, ";
                $mcu = $str;
            }else{
                $mcu = '-';
            }

            $barang = DB::table('order_assets')
                      ->select('assets.nama_barang', 'order_assets.qty')
                      ->join('assets', 'assets.id' , '=', 'order_assets.asset_id')
                      ->where('order_assets.order_id', $value->id)->get();
            if(count($barang) > 0){
                foreach($barang as $b){
                    $str_barang .= "Nama Barang : {$b->nama_barang} Qty : {$b->qty}, ";
                }
                $list_barang = $str_barang;
            }else{
                $list_barang = '-';
            }
               
            @endphp
            
            <td>{{ $mcu }}</td>
            <td>{{ $list_barang }}</td>

          </tr>
          @php
            $no++;
          @endphp
        @empty

        @endforelse
      </tbody>
    </table>

    Current Page: {{ $data->currentPage() }}<br>
    Jumlah Data: {{ $data->total() }}<br>
    Data perhalaman: {{ $data->perPage() }}<br>
    <br>
    {{ $data->appends($params)->links() }}
  </div>
  </div>

  <!-- The Modal -->

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" data-dismiss="modal">
    <div class="modal-content"  >
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>


{{-- modal export --}}
<!-- Modal -->
<div class="modal fade" id="modalExportPdf">
  <div class="modal-dialog">
    <div class="modal-content">
    <!-- header-->
      <div class="modal-header">
        <button class="close" data-dismiss="modal"><span>&times;</span></button>
        <h4 class="modal-title">Export</h4>
      </div>
      <!--body-->
      <div class="modal-body">
        <form class="" action="{{route('orderListExport')}}" method="post">
          @csrf
          <div class="form-group">
            <label for="email">Start Date:</label>
            <input type="date" name="start_date" class="form-control @error('start_date') is-invalid @enderror">
            @error('start_date')
              <div class="invalid-feedback br">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="email">End Date:</label>
            <input type="date" name="end_date" class="form-control @error('end_date') is-invalid @enderror">
            @error('end_date')
              <div class="invalid-feedback br">{{ $message }}</div>
            @enderror
          </div>
          <button type="submit" class="btn btn-primary" name="button">Export</button>
        </form>
      </div>
      <!--footer-->
    </div>
  </div>
</div>
@endsection


@push('bottom')
  <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>

  <script src="https://cdn.datatables.net/fixedheader/3.2.2/js/dataTables.fixedHeader.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>

<script type="text/javascript">
$("#btnModeClose").on("click", function (e) {
        e.preventDefault();
        $('#imagemodal').modal('hide');
});
$(function() {
  $('.pop').on('click', function() {
    $('.imagepreview').attr('src', $(this).find('img').attr('src'));
    $('#imagemodal').modal('show');
  });
});
</script>
<script type="text/javascript">
$(function () {
      var table = $('#dataTable').DataTable({
          "responsive": true,
           paging: false,
           ordering: false,
           info: false,
           searching: false,
      });
});
</script>
@endpush
