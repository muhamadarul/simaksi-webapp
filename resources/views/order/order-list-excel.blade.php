<table>
  <thead>
    <tr>
      <th colspan="3">Laporan Simaksi {{ $start }} s.d {{ $end }}</th>
    </tr>
    <tr>
    <th>No</th>
        <th>Pendaki</th>
        <th>Email</th>
        <th>Order Code</th>
        <th>Tgl Naik</th>
        <th>Tgl Turun</th>
        <th>Persetujuan</th>
        <th>Disetujui Oleh</th>
        <th>Check in</th>
        <th>Check out</th>
        <th>Hasil MCU</th>
        <th>Sewa Barang</th>
    </tr>
  </thead>
  @php
    $no = 1;
  @endphp
  <tbody>
    @forelse ($data as $key => $value)
    <tr>
            <td>{{$no}}</td>
            <td>{{$value->pendaki}}</td>
            <td>{{$value->email_pendaki}}</td>
            <td>{{$value->order_code}}</td>
            <td>{{$value->tgl_naik}}</td>
            <td>{{$value->tgl_turun}}</td>
            @if ($value->persetujuan == 1)
            <td>Disetujui</td>
            @else
              <td>-</td>
            @endif
            <td>{{$value->disetujui_oleh}}</td>
            <td>{{$value->check_in}}</td>
            <td>{{$value->check_out}}</td>
            @php
            $str = "";
            $str_barang = "";

            $mcu_data = DB::table('mcu_users')->where('order_id', $value->id)->first();

            if($mcu_data){
                $str .= "Tensi darah : {$mcu_data->tensi_darah}, ";
                $str .= "Tinggi badan : {$mcu_data->tinggi_badan}, ";
                $str .= "Berat badan : {$mcu_data->berat_badan}, ";
                $str .= "Keterangan : {$mcu_data->keterangan}, ";
                $str .= "Diperiksa oleh : {$mcu_data->diperiksa_oleh}, ";
                $mcu = $str;
            }else{
                $mcu = '-';
            }

            $barang = DB::table('order_assets')
                      ->select('assets.nama_barang', 'order_assets.qty')
                      ->join('assets', 'assets.id' , '=', 'order_assets.asset_id')
                      ->where('order_assets.order_id', $value->id)->get();
            if(count($barang) > 0){
                foreach($barang as $b){
                    $str_barang .= "Nama Barang : {$b->nama_barang} Qty : {$b->qty}, ";
                }
                $list_barang = $str_barang;
            }else{
                $list_barang = '-';
            }
               
            @endphp
            
            <td>{{ $mcu }}</td>
            <td>{{ $list_barang }}</td>

          </tr>
      @php
        $no++;
      @endphp
    @empty

    @endforelse
  </tbody>
</table>
