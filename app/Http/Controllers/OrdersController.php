<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Illuminate\Support\Facades\Validator;
use App\Exports\OrderListExport;

class OrdersController extends Controller
{
    public function orderList(Request $request){

        $params = $request->all();
    
        $data = DB::table('orders')
                ->join('users', 'users.id', 'orders.user_id')
                ->select('orders.*','users.name as pendaki', 'users.email as email_pendaki')
                ->where('orders.persetujuan', 1);
    

         if ($request->has('keyword')) {
           if (!$request->start_date && !$request->end_date) {
             $data = $data->where(function ($query) use ($request) {
                   $query->where('orders.order_code', 'like', $request->keyword.'%')
                         ->orWhere('orders.order_code', 'like', '%'.$request->keyword)
                         ->orWhere('users.name', 'like', '%'.$request->keyword.'%')
                         ->orWhere('users.name', 'like', '%'.$request->keyword);
               });
    
               $data = $data->orderBy('orders.id', 'desc')->paginate(15);
    
           }elseif($request->start_date && $request->end_date) {
             $data = $data->where(function ($query) use ($request) {
                   $query->where('orders.order_code', 'like', $request->keyword.'%')
                         ->orWhere('orders.order_code', 'like', '%'.$request->keyword)
                         ->orWhere('users.name', 'like', '%'.$request->keyword.'%')
                         ->orWhere('users.name', 'like', '%'.$request->keyword);
               });
             $data =  $data->whereDate('orders.check_in','>=', $request->start_date)
                            ->whereDate('orders.check_in','<=', $request->end_date);
    
             $data = $data->orderBy('orders.id', 'desc')->paginate(15);
    
           }
         }else {
           $data = $data->orderBy('orders.id', 'desc')->paginate(15);
         }
         return view('order.order-list', compact('data','params'));
      }

      public function orderListExport(Request $request)
      {
        $validator = Validator::make($request->all(), [
             'start_date' => 'required',
             'end_date' => 'required',
          ]);
    
          if ($validator->fails()) {
                 return back()
                 ->withErrors($validator)
                 ->withInput();
          }

            
        return Excel::download(new OrderListExport($request->start_date,$request->end_date),'simaksi-list('.$startDate.'-sd-'.$endDate.').xls');
      }
}
