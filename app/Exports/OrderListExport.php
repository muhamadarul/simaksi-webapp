<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View; //Harus diimport untuk men-convert blade menjadi file excel
use Maatwebsite\Excel\Concerns\FromView;
use DB;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class OrderListExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(string $start, string $end)
    {
        $this->start = $start;
        $this->end = $end;
    }
    public function view(): View
    {
        $data = DB::table('orders')
                ->join('users', 'users.id', 'orders.user_id')
                ->select('orders.*','users.name as pendaki', 'users.email as email_pendaki')
                ->where('orders.persetujuan', 1)
                ->whereDate('orders.tgl_naik','>=', $this->start)
                ->whereDate('orders.tgl_naik','<=', $this->end);

   

        $data = $data->orderBy('orders.id', 'desc')->get();


      return view('order.order-list-excel',[
        'data' => $data,
        'start' => $this->start,
        'end' => $this->end,
      ]);
    }
}
