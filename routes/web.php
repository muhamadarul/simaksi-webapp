<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrdersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  abort(404);
  });

  Route::get('admin/report-simaksi', [OrdersController::class, 'orderList'])->name('orderList');
  Route::post('admin/simaksi-export', [OrdersController::class, 'orderListExport'])->name('orderListExport');


